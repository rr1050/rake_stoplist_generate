from collections import Counter
import re
import hazm_decorator


class DocumentMiner:
    def __init__(self, content, title=None, human_keywords=None):

        if not isinstance(content, str):
            raise TypeError("content must be string")
        else:
            self.content = hazm_decorator.get_normalized_text(content)

        if title:
            if isinstance(title, str):
                self.title = hazm_decorator.get_normalized_text(title)
            else:
                raise TypeError("title must be string")
        else:
            self.title = ''

        if human_keywords:
            if isinstance(human_keywords, list):
                self.human_keywords = [hazm_decorator.get_normalized_text(k) for k in human_keywords]
            else:
                raise TypeError("human keywords must be list")
        else:
            self.human_keywords = None

        self.document_words = None

    def get_words(self):
        if self.document_words is None:
            self.document_words = re.findall(r'\w+', self.title + self.content)
        return self.document_words

    def get_words_count_list(self):
        return [(word, count) for word, count in Counter(self.get_words()).most_common()]

    def get_keywords_adjacencies(self):
        if self.human_keywords is None:
            raise ValueError("human keywords is not defined")
        main_result = []

        sentence_list = hazm_decorator.get_all_sentences(self.title + self.content)
        for keyword in self.human_keywords:
            for sentence in sentence_list:
                keyword_list = re.findall(r'\w+', keyword)
                keyword_list_length = len(keyword_list)
                words = re.findall(r'\w+', sentence)
                words_length = len(words)
                found_indexes = get_item_indexes(words, keyword_list[0])
                result_list = []
                for index in found_indexes:
                    flag = True
                    for i in range(keyword_list_length):
                        if words_length > (index + i):
                            if keyword_list[i] != words[index + i]:
                                flag = False
                        else:
                            flag = False

                    if flag is True:
                        before_elem = words[index - 1] if index != 0 else ""
                        after_index = index + keyword_list_length
                        after_elem = words[after_index] if words_length > after_index else ""
                        result = (keyword, before_elem, after_elem)
                        result_list.append(result)
                if len(result_list) > 0:
                    for item in result_list:
                        main_result.append(item)

        return main_result


def get_item_indexes(search_list, elem):
    return [i for i, x in enumerate(search_list) if x == elem]


    # def get_keyword_adjacencies(self, keyword):
    #     keyword_list = re.findall(r'\w+', keyword)
    #     keyword_list_length = len(keyword_list)
    #     words = self.get_words()
    #     words_length = len(words)
    #     found_indexes = get_item_indexes(words, keyword_list[0])
    #     # if len(found_indexes) < 1:
    #     #     return []
    #
    #     result_list = []
    #     for index in found_indexes:
    #         flag = True
    #         for i in range(keyword_list_length):
    #             if words_length > (index + i):
    #                 if keyword_list[i] != words[index + i]:
    #                     flag = False
    #             else:
    #                 flag = False
    #
    #         if flag is True:
    #             before_elem = words[index - 1] if index != 0 else ""
    #             after_index = index + keyword_list_length
    #             after_elem = words[after_index] if words_length > after_index else ""
    #             result = (keyword, before_elem, after_elem)
    #             result_list.append(result)
    #
    #     return result_list



    # def get_keywords_adjacencies(self):
    #     if self.human_keywords is None:
    #         raise ValueError("human keywords is not defined")
    #     result_list = []
    #     for item in self.human_keywords:
    #         for k in self.get_keyword_adjacencies(item):
    #             result_list.append(k)
    #     return result_list

    # def get_keywords_adj(self):
    #     if self.human_keywords is None:
    #         raise ValueError("human keywords is not defined")
    #     main_result = []
    #
    #     sentence_list = hazm_decorator.get_all_sentences(hazm_decorator.get_normalized_text(self.content))
    #     for sentence in sentence_list:
    #         for keyword in self.human_keywords:
    #             keyword_list = re.findall(r'\w+', keyword)
    #             keyword_list_length = len(keyword_list)
    #             words = re.findall(r'\w+', sentence)
    #             words_length = len(words)
    #             found_indexes = get_item_indexes(words, keyword_list[0])
    #             result_list = []
    #             for index in found_indexes:
    #                 flag = True
    #                 for i in range(keyword_list_length):
    #                     if words_length > (index + i):
    #                         if keyword_list[i] != words[index + i]:
    #                             flag = False
    #                     else:
    #                         flag = False
    #
    #                 if flag is True:
    #                     before_elem = words[index - 1] if index != 0 else ""
    #                     after_index = index + keyword_list_length
    #                     after_elem = words[after_index] if words_length > after_index else ""
    #                     result = (keyword, before_elem, after_elem)
    #                     result_list.append(result)
    #             if len(result_list) > 0:
    #                 main_result.append(result_list)
    #     return main_result



