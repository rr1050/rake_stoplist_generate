from builtins import print
import time
import web_site_tools as site_tools
import datetime
from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner



#
# #create database
# Base.metadata.create_all(engine)
# session = Session()
# session.commit()
# session.close()

