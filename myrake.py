#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Jul 29 19:57:01 2018

@author: mohsen
"""


import hazm

tagger = hazm.POSTagger(model='resources/postagger.model')



file = open('shajarian', 'r')
news = file.read()
body = news
rake_text = news
file = open('stopwords.dat','r')
stopwords = file.read()

stopwords = stopwords.split('\n')
special = ['.',';','،','،','؛','«','»',':']
for item in special:
    news = news.replace(item, ' {}'.format(item))



# generate candid list(every 2 words between stop words)
news = news.split(' ')
keywords = []

for i in range(len(news)):
    if news[i] in stopwords:
        for j in range(i+1, len(news)):
            if news[j] in stopwords:
                text = news[i+1:j]
                
                text = ' '.join(text)
                if len(text) > 1:
                    for item in special:
                        text = text.replace(item, '')
                    keywords.append(text)
                break


# word tokenizing the news
# dont count adverbs
for item in special:
    body = body.replace(item, '')
    
wordlist = hazm.word_tokenize(body)
wordlist2 = set(wordlist)
wordlist = []
for item in wordlist2:
    if item not in stopwords:
        tag = tagger.tag(hazm.word_tokenize(item))
        if 'ADV' not in tag[0]:
            wordlist.append(item)
            
            
# computing frequency for each word
freq = 0
tmp = ' '.join(keywords)
word_freq = {}

for word in wordlist:
    word_freq[word] = tmp.count(word)

# comuting degre for each word
deg = 0
word_deg = {}
for word in wordlist:
    for key in keywords:
        if word in key:
            deg += len(key)
    word_deg[word] = deg
    deg = 0


# computing score for word
score = {}
for word in wordlist:
    if word_freq[word] == 0:
        word_freq[word] = 1
    score[word] = word_deg[word] / word_freq[word]
    
# computing score for every candid by word score
scores = 0
candid_dict = {}
for candid in keywords:
    tmp = candid.split(' ')
    for key in tmp:
        if key in score:
            scores += int(score[key])
    if len(tmp) < 4:
        candid_dict[candid] = scores
    scores = 0
    
#------------------------------------------------------------------------------


from rake_nltk import Rake

r = Rake(stopwords, min_length=1, max_length=3) 
r.extract_keywords_from_text(rake_text)
orginal_rake = r.get_ranked_phrases_with_scores()





#
#
#tagger = hazm.POSTagger(model='resources/postagger.model')
#tag = tagger.tag(hazm.word_tokenize('بارها'))
#if 'AJ' in tag[0]:
#    print(2)
#body.count('بارها')
#body.count('محبوب')
##

#
#
#import RAKE
#Rake = RAKE.Rake(stopwords)
#rake_scores = Rake.run('کد ویدئو دانلود ')





