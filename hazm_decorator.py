from __future__ import unicode_literals
from hazm import *
import re

tagger = POSTagger(model='resources/postagger.model1')
chunker = Chunker(model='resources/chunker.model1')


def get_normalized_text(text):
    """
    اصلاح کاراکترهای فارسی و ویرایش متن بر اساس قواعد نوشتاری فارسی

    :param text: متن اصلاح نشده
    :return: ‌متن اصلاح شده
    """
    normalizer = Normalizer()
    result = normalizer.normalize(text)
    return result


def get_all_sentences(text):
    """
    استخراج تمام جملات متن

    :param text: متن ورودی
    :return: لیست تمام جملات متن
    """
    return sent_tokenize(text)


def get_words(text):
    """
    استخراج تمام کلمات متن

    :param text: متن ورودی
    :return: لیست تمام کلمات متن
    """
    return word_tokenize(text)


def get_word_chanks(text):
    """
    استخراج تمام کلمات متن

    :param text: متن ورودی
    :return: لیستی از تمامی کلمات
    """
    return tagger.tag(get_words(text))


def get_sentenses_chank(text):
    """
    استخراج تمام جملات متن

    :param text: متن ورودی
    :return: جملات متن بصورت رشته
    """
    tagged = tagger.tag(word_tokenize(text))
    return tree2brackets(chunker.parse(tagged))


def get_sentenses_chank_list(text):
    """
    لیست تمام جملات متن

    :param text:متن ورودی
    :return: لیست جملات متن
    """
    str = get_sentenses_chank(text)
    return re.findall(r'\[(.*?)\]', str)


def get_noun_phrases_chank_list(text):
    """
    استراج عبارات اسمی متن

    :param text: متن ورودی
    :return: لیست عبارات اسمی متن
    """
    list = get_sentenses_chank_list(text)
    return [s.replace(' NP', '') for s in list if s.endswith('NP')]

