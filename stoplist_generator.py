import ast

all_words = []
with open('resources/word_details.txt', 'r') as filehandle:
    all_words = [ast.literal_eval(current_place.rstrip()) for current_place in filehandle.readlines()]


def get_stop_list(min_df=1):
    return [w['word'] for w in all_words if w['DF'] >= min_df and w['KF'] <= w['AF']]


# for i in range(30):
#     print("for DF  " + str(i))
#     stop_list = get_stop_list(i)
#     print(len(stop_list))
#     print("---------------------------")


# stop_list = get_stop_list(30)
# print(len(stop_list))

# for item in list:
#     print(item)


