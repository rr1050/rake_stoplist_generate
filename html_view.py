from flask import Flask
from flask import render_template
app = Flask(__name__)

from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner



@app.route('/')
def hello_world():
    session = Session()
    words_query = """SELECT df, AVG(news_precision) AS avg_precision , 
AVG(news_recall) avg_recall,
 AVG(news_fmeasure) AS avg_fmeasure FROM `news_rake` GROUP by df"""
    words = session.execute(words_query)
    # print(list(words))
    words = list(words)
    word1 = []
    word2 = []
    word3 = []
    for i in words:
        word1.append(i[1])
        word2.append(i[2])
        word3.append(i[3])

    return render_template('df.html', precision=word1, recall=word2, fmeasure=word3)
