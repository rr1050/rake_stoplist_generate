import stoplist_generator
from rake_nltk import Rake
from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner
from sqlalchemy.orm import joinedload
import my_rake

# punctuations_list = ['!',',','.',':',';','،','؛','«','»','(', ')','؟']


def evaluate_keywords(human_keyword, extracted_keyword):
    try:
        human_keyword = set([k.replace(' ', '').replace("\u200c", "") for k in human_keyword])
        extracted_keyword = set([k.replace(' ', '').replace("\u200c", "") for k in extracted_keyword])
        human_keyword_length = len(human_keyword)

        tp_list = human_keyword & extracted_keyword
        tp_length = len(tp_list)

        precision = tp_length / (tp_length + len(extracted_keyword) - tp_length)
        recall = tp_length / (tp_length + human_keyword_length - tp_length)
        if (recall + precision) == 0:
            return 0, 0, 0
        extracted_fmeasure = (2 * ((precision * recall)/(recall + precision)))

        return precision, recall, extracted_fmeasure

    except:
        return 0, 0, 0


session = Session()
all_news = session.query(News).options(joinedload(News.keywords)).all()
session.close()


#
# for news in all_news:
#     human_keywords = [k.keyword_name for k in news.keywords]
#     news_id = news.id
#     print(news_id)
#
#     for i in range(1, 31):
#         stop = stoplist_generator.get_stop_list(i)
#
#         my_rake_keywords = my_rake.get_top_phrase(news.content, stopwords=stop, max_phrase_length=3)[:10]
#
#         precision, recall, fmeasure = evaluate_keywords(human_keywords, my_rake_keywords)
#
#         session = Session()
#         words_query = """
#     insert into news_my_rake (news_id, df, news_precision,news_recall, news_fmeasure)
#     VALUES ({},{},{},{},{})
#     """.format(news_id, i, precision, recall, fmeasure)
#
#         session.execute(words_query)
#         session.commit()











# session = Session()
# all_news = session.query(News).filter(News.id == 1).options(joinedload(News.keywords)).all()
# session.close()
#
# for news in all_news:
#     human_keywords = [k.keyword_name for k in news.keywords]
#     news_id = news.id
#     print(news.link)
#
#     for i in range(1, 30):
#         stop = stoplist_generator.get_stop_list(i)
#
#         my_r =  my_rake.get_top_phrase(news.content, stopwords=stop,max_phrase_length=3)[:10]
#         print(my_r)
#
#         r = Rake(stopwords=stop, punctuations=punctuations_list, max_length=3)
#         r.extract_keywords_from_text(news.content)
#
#
#         print(i)
#
#         li =  r.get_ranked_phrases()[:(len(human_keywords) + 5)]
#         print(li)
#         print("---------------------")
#
#         precision, recall, fmeasure = evaluate_keywords(human_keywords,
#                                                         r.get_ranked_phrases()[:(len(human_keywords) + 5)])
#
#
#         print(i)
#         print("precision")
#         print(precision)
#         print("recall")
#         print(recall)
#         print("fmeasure")
#         print(fmeasure)
#         print("---------------------")





