from builtins import print
import time
import web_site_tools as site_tools
import datetime
from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner
from sqlalchemy.orm import joinedload
import  my_rake
import stoplist_generator




#
# link = "http://www.tabnak.ir/fa/news/818697"
# html = site_tools.get_html(link)
# content = '\n'.join(site_tools.get_list_by_css_selector(html, "div.body"))
# title = site_tools.get_by_css_selector(html, "h1.Htag a")
# abstract = site_tools.get_by_css_selector(html, "div.subtitle")
# keywords = site_tools.get_list_by_css_selector(html, "div.tag_items a")
# categories = site_tools.get_list_by_css_selector(html, "div.news_path a")
#
# d = DocumentMiner(content, title=title, human_keywords=keywords)
# for item in d.get_keywords_adjacencies():
#     print(item)
# print(len(d.get_keywords_adjacencies()))

#
# li = d.get_keywords_adjacencies()
# for i in li:
#     print(i)
#
# print(len(li))


#
# link = "http://www.tabnak.ir/fa/news/" + str(818696)
#
# html = site_tools.get_html(link)
# content = '\n'.join(site_tools.get_list_by_css_selector(html, "div.body"))
# title = site_tools.get_by_css_selector(html, "h1.Htag a")
# abstract = site_tools.get_by_css_selector(html, "div.subtitle")
# keywords = site_tools.get_list_by_css_selector(html, "div.tag_items a")
# categories = site_tools.get_list_by_css_selector(html, "div.news_path a")






#
# c = 818690
# for x in range(c, c - 1100, -1):
#     print(x)
#     link = "http://www.tabnak.ir/fa/news/" + str(x)
#
#     html = site_tools.get_html(link)
#     content = '\n'.join(site_tools.get_list_by_css_selector(html, "div.body"))
#     title = site_tools.get_by_css_selector(html, "h1.Htag a")
#     abstract = site_tools.get_by_css_selector(html, "div.subtitle")
#     keywords = site_tools.get_list_by_css_selector(html, "div.tag_items a")
#     categories = site_tools.get_list_by_css_selector(html, "div.news_path a")
#
#
#
#     keyword_adjacencies = content_tools.get_keyword_adjacency(content, keywords)
#     print(keyword_adjacencies)

#
# content_text = "یک دو سه چهار. پنج شش هفت! هشت نه ده یازده"
#
# for item in content_text.split(" "):
#     print("for keyword...   " + item)
#     keyword_adjacencies = content_tools.get_keyword_adjacency(content_text, [item])
#     print(keyword_adjacencies)
#     print("-------")
#     c = DocumentMiner(content_text, human_keywords=[item])
#     for i in c.get_keywords_adjacencies():
#         print(i)
#     print("********************************")



session = Session()
all_news = session.query(News).filter(News.id == 17).options(joinedload(News.keywords)).all()
session.close()

for news in all_news:
    print("--------------------")
    human_keywords = [k.keyword_name for k in news.keywords]
    print(human_keywords)
    print(news.link)
    # print(news.content)

    stop = stoplist_generator.get_stop_list()
    my_rake_keywords = my_rake.get_top_phrase(news.content, stopwords=stop, max_phrase_length=3)
    print(my_rake_keywords)