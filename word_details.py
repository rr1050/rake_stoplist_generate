from builtins import print
import time
import web_site_tools as site_tools
import datetime
from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner



session = Session()
words_query = """SELECT * FROM 
(SELECT word, SUM(count) as TF, count(word) as DF FROM news_words WHERE news_id <= 1000 GROUP by word) a
 ORDER by TF DESC"""

words = session.execute(words_query)

print(len(list(words)))

keywords_query = """SELECT * FROM 
(SELECT keyword_name, COUNT(keyword_name) as count FROM `news_keywords` WHERE news_id <= 1000 and keyword_type = 0 GROUP BY keyword_name) a 
ORDER by a.count DESC"""

keyword_list = session.execute(keywords_query)


keyword_list_score = []
for item in keyword_list:
    keyword_words = item[0].split(' ')
    for word in keyword_words:
        keyword_list_score.append((word, item[1]))

# su = 0
# for item in keyword_list_score:
#     if "ایران" == item[0]:
#         su += int(item[1])
#
# print(su)


keywords_adj_query = """SELECT * FROM
(SELECT keyword_adjacencies.title , COUNT(keyword_adjacencies.title) as ajd_cont FROM `keyword_adjacencies` JOIN news_keywords on keyword_adjacencies.news_keyword_id = news_keywords.id
WHERE news_keywords.news_id <= 1000
GROUP BY keyword_adjacencies.title) as a
ORDER BY a.ajd_cont DESC"""

keywords_adj_list = session.execute(keywords_adj_query)
keywords_adj_list = list(keywords_adj_list)



# result_list = []
# for item in list(words):
#
#     word = item[0]
#     word_kf = 0
#     for keyword_score in keyword_list_score:
#         if word == keyword_score[0]:
#             word_kf += keyword_score[1]
#     word_af = 0
#     for keywords_adj in keywords_adj_list:
#         if word == keywords_adj[0]:
#             word_af += int(keywords_adj[1])
#
#     dic_row = {"word": word, "TF": int(item[1]), "DF": item[2], "AF": word_af, "KF": word_kf}
#     result_list.append(dic_row)
#
#
# print("writing to file...")
# with open('resources/word_details.txt', 'w+') as filehandle:
#     filehandle.writelines("{}\n".format(place) for place in result_list)
