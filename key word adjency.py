def extract_adjency(news, keyWords):
    special_char_list = ['«', '»', ':',';', '.']

    # adding space after and before of keywords
    for i in range(len(keyWords)):
        tmp = keyWords[i]
        tmp = ' ' + tmp + ' '
        # tmp = list(tmp)
        # tmp.insert(0, ' ')
        # tmp.append(' ')
        # tmp = ''.join(tmp)
        keyWords[i] = tmp
    #is is neccessery to strip keyword before above for loop?
 
    #deleting special char from text
    for item in special_char_list:
        news = news.replace(item, '')
    
    
    #making a dict from keywords with unique values
    keydict = {}
    i = 0
    for item in keyWords:
        i += 1
        keydict[item] = "key{}".format(i)
        
        
    #replacing keywords in the text with their unique value from dic    
    for key, val in keydict:
        news = news.replace(key, ' {} '.format(val))
    
 
    
    #find adjency of the keywords in the text            
    news = news.split(' ')
    
    adjenList = []
    for i in range(len(news)):
        if 'key' in news[i]:
            before = news[i-1]
            after = news[i+1]
            if 'key' in before:
                before = list(keydict.keys())[list(keydict.values()).index(news[i-1])]
            if 'key' in after:
                after = list(keydict.keys())[list(keydict.values()).index(news[i+1])]
            adjenList.append((list(keydict.keys())[list(keydict.values()).index(news[i])],
                              after,
                              before))
            
            

  