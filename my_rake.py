
def get_top_phrase(text, stopwords, max_phrase_length=20):

    punctuations = '!:\.،؛؟»\]\)\}ـ-_«\[\(\{.'
    for char in punctuations:
        text = text.replace(char, ' ' + char + ' ')

    splited_text = [item for item in text.replace('\n', '').split(' ') if item != '' and item != ' ']

    candidate_phrases = []
    new_phrase = ''
    i = len(splited_text)
    for x in range(0, i):
        if splited_text[x] not in stopwords + list(punctuations):
            if new_phrase == '':
                new_phrase = splited_text[x]
            else:
                new_phrase = new_phrase + " " + splited_text[x]
        else:
            if new_phrase != '':
                candidate_phrases.append(new_phrase)
                new_phrase = ''

    candidate_words = []
    for candidate in candidate_phrases:
        for candidate_word in candidate.split(" "):
            candidate_words.append(candidate_word)

    words_score = []
    for word in set(candidate_words):
        contained_candidate = [(c.split(' ')) for c in candidate_phrases if word in (c.split(' '))]
        word_deg = sum([len(c) for c in contained_candidate])
        word_freq = len(contained_candidate)
        word_score = word_deg / (word_freq if word_freq != 0 else 1)
        words_score.append((word, word_deg, word_freq, word_score))

    phrase_list = []
    for new_phrase in set(candidate_phrases):
        phrase_score = 0
        for word in new_phrase.split(' '):
            score = [s[3] for s in words_score if s[0] == word][0]
            phrase_score += score

        phrase_list.append((new_phrase, phrase_score))

    if max_phrase_length < 20:
        phrase_list = [p for p in phrase_list if len(p[0].split(' ')) <= max_phrase_length]

    sorted_list = sorted(phrase_list, key=lambda tup: tup[1], reverse=True)
    return [a for a, b in sorted_list]


