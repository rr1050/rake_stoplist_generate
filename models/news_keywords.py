from sqlalchemy import Column, String, Integer,Float, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base


class NewsKeyword(Base):
    __tablename__ = 'news_keywords'

    id = Column(Integer, primary_key=True)
    news_id = Column(Integer, ForeignKey('news.id'))
    news = relationship("News", back_populates="keywords")
    keyword_name = Column(String(1000000))
    keyword_score = Column(Float, nullable=True)
    keyword_type = Column(Integer)
    adjacencies = relationship("KeywordAdjacency", back_populates="news_keyword")

    def __init__(self, keyword_name, keyword_score, keyword_type):
        self.keyword_name = keyword_name
        self.keyword_score = keyword_score
        self.keyword_type = keyword_type

