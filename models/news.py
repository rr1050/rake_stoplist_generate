from sqlalchemy import Column, String, Integer, DateTime, Boolean
from sqlalchemy.orm import relationship
from models.base import Base


class News(Base):
    __tablename__ = 'news'

    id = Column(Integer, primary_key=True)
    link = Column(String(10000))
    title = Column(String(10000))
    abstract = Column(String(100000))
    content = Column(String(10000000))
    insert_date = Column(DateTime)
    keywords = relationship("NewsKeyword", back_populates="news")
    categories = relationship("NewsCategory", back_populates="news")
    words = relationship("NewsWord", back_populates="news")
    error = Column(Boolean)

    def __init__(self, link,title,content,abstract, insert_date, error=False):
        self.link = link
        self.title = title
        self.content = content
        self.insert_date = insert_date
        self.error = error
        self.abstract = abstract
