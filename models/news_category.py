from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base


class NewsCategory(Base):
    __tablename__ = 'news_category'

    id = Column(Integer, primary_key=True)
    news_id = Column(Integer, ForeignKey('news.id'))
    news = relationship("News", back_populates="categories")
    title = Column(String(1000000))

    def __init__(self, title):
        self.title = title


