from sqlalchemy import Column, String, Integer,Float, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base


class KeywordAdjacency(Base):
    __tablename__ = 'keyword_adjacencies'

    id = Column(Integer, primary_key=True)
    news_keyword_id = Column(Integer, ForeignKey('news_keywords.id'))
    news_keyword = relationship("NewsKeyword", back_populates="adjacencies")
    title = Column(String(100))
    type = Column(Integer)

    def __init__(self, title, type):
        self.title = title
        self.type = type

