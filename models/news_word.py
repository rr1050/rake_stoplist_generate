from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from models.base import Base


class NewsWord(Base):
    __tablename__ = 'news_words'

    id = Column(Integer, primary_key=True)
    news_id = Column(Integer, ForeignKey('news.id'))
    news = relationship("News", back_populates="words")
    word = Column(String(1000))
    count = Column(Integer)

    def __init__(self, word, count):
        self.word = word
        self.count = count


