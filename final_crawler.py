from builtins import print
import time
import web_site_tools as site_tools
import datetime
from models.base import Session, engine, Base
from models.news import News
from models.news_keywords import NewsKeyword
from models.news_category import NewsCategory
from models.keyword_adjacency import KeywordAdjacency
from models.news_word import NewsWord
import content_tools
from document_miner import DocumentMiner




print("-------------")

Base.metadata.create_all(engine)

c = 817090
for x in range(c, c - 7000, -1):
    print(x)
    link = "http://www.tabnak.ir/fa/news/" + str(x)

    html = site_tools.get_html(link)
    content = '\n'.join(site_tools.get_list_by_css_selector(html, "div.body"))
    title = site_tools.get_by_css_selector(html, "h1.Htag a")
    abstract = site_tools.get_by_css_selector(html, "div.subtitle")
    keywords = site_tools.get_list_by_css_selector(html, "div.tag_items a")
    categories = site_tools.get_list_by_css_selector(html, "div.news_path a")

    new_file = open("resources/tabnak_files/{}.html".format(x), "w+")
    new_file.write(str(html))
    new_file.close()

    if len(keywords) > 4:
        s = time.time()

        session = Session()
        news = News(link, title, content, abstract, datetime.datetime.now())

        news_categories = []
        for item in categories:
            c = NewsCategory(item)
            news_categories.append(c)
        news.categories = news_categories

        document_miner = DocumentMiner(content, title=title, human_keywords=keywords)
        keyword_adjacencies = document_miner.get_keywords_adjacencies()

        news_keywords = []
        i = len(keywords)
        for keyword in document_miner.human_keywords:
            k = NewsKeyword(keyword, i, 0)
            i -= 1

            news_keyword_adjacencies = []
            for adjacency in [t for t in keyword_adjacencies if t[0].strip() == keyword]:
                if adjacency[1] != '':
                    bka = KeywordAdjacency(adjacency[1], -1)
                    news_keyword_adjacencies.append(bka)
                if adjacency[2] != '':
                    aka = KeywordAdjacency(adjacency[2], 1)
                    news_keyword_adjacencies.append(aka)

            k.adjacencies = news_keyword_adjacencies
            news_keywords.append(k)

        news.keywords = news_keywords

        news_words = []
        for item in document_miner.get_words_count_list():
            news_word = NewsWord(item[0], item[1])
            news_words.append(news_word)
        news.words = news_words

        session.add(news)
        session.commit()
        session.close()
        print(time.time() - s)