from collections import Counter
import re


def get_keyword_adjacency(content, keywords):
    
#    text_file = open("newstext", "r")
#    content = text_file.read()
#    #content = content.strip()
#    keywords = ['فغانی']
    special_char_list = ['«', '»', ':', ';', '،']

    # adding space after and before of keywords
    for i in range(len(keywords)):
        tmp = keywords[i]
        tmp = tmp + ' '
        keywords[i] = tmp
    # is is neccessery to strip keyword before above for loop?

    # deleting special char from text
    for item in special_char_list:
        content = content.replace(item, '')

    # making a dict from keywords with unique values
    key_identifiaction = {}
    keydict = {}
    i = 0
    for item in keywords:
        i += 1
        keydict[item] = "key{}".format(i)
        key_identifiaction['key{}'.format(i)] = item
    # replacing keywords in the text with their unique value from dic
    tmp = content
    content = []
    tmp = tmp.split('\n')
    for i in range(len(tmp)):
        if len(tmp[i]) > 2:
            content.append(tmp[i])
            
    content = ' '.join(content)
    for key in keydict:
        content = content.replace(key, '{} '.format(keydict[key]))

        # find adjency of the keywords in the text
   
    content = content.replace('.', ' null')
    content = content.split(' ')

    adjenList = []
    for i in range(len(content)):
        if 'key' in content[i]:
            before = content[i - 1]
            after = content[i + 1]
            if 'key' in before:
                if i > 0 and '.'not in content[i-1]:
                    before = key_identifiaction[content[i - 1]]
                else:
                    before = 'null'
            if 'key' in after and '.' not in content[i+1]:
                if key < len(content):
                    after = key_identifiaction[content[i + 1]]
                else:
                    after = 'null'
            adjenList.append((key_identifiaction[content[i]],
                              after,
                              before))
            
            
            

    return adjenList


def get_content_words(content):
    return [(word, count) for word, count in Counter(re.findall(r'\w+', content)).most_common()]
