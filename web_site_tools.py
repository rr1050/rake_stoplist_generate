import feedparser
from bs4 import BeautifulSoup
import requests

def get_feed_links(rss_link):
    """
    خواندن تمام لینک ها از آدرس فید داده شده
    :param rss_link: آدرس فید سایت
    :return: لیست لینک تمامی مطالب از فید مورد نظر
    """
    feed = feedparser.parse(rss_link)
    links = []
    for entry in feed.entries:
        links.append(entry.link)
    return links


# def ascii_url(non_ascii_url):
#     """
#     تبدیل لینک غیر اسکی به لینک با کاراکترهای اصلی
#     :param non_ascii_url: لینک شامل کاراکترهای غیر اسکی
#     :return: لینک با کاراکترهای اسکی شده
#     """
#     url = urllib.parse.urlsplit(non_ascii_url)
#     url = list(url)
#     url[2] = urllib.parse.quote(url[2])
#     url = urllib.parse.urlunsplit(url)
#     return url


def get_string_form_page(url):
    return requests.get(url).text
    # response = urllib.request.urlopen(url)
    # page_content = response.read().decode(response.headers.get_content_charset(),  errors='replace')
    # return page_content


def html_parser(content):
    soup = BeautifulSoup(content, "html.parser")
    for script in soup(["script", "style"]):
        script.extract()
    return soup


def get_html(url):
    return html_parser(get_string_form_page(url))


def get_list_by_css_selector(html, selector):
    result = []
    for l in html.select(selector):
        result.append(l.text.strip())
    return result


def get_by_css_selector(html, selector):
    if len(html.select(selector)) > 0:
        return html.select(selector)[0].text.strip()
    else:
        return ""

